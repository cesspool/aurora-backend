﻿const express = require('express');
const app = express();
const port = 3000;
var bodyParser=require('body-parser')
var url = 'mongodb://68.183.214.110/27017';
var MongoClient = require('mongodb').MongoClient;
// TÃ¤tÃ¤ tarvitaan, jos post-metodilla lÃ¤hetetÃ¤Ã¤n parametreja. 
// Ei siis tÃ¤ssÃ¤ esimerkissÃ¤ tarpeellinen, mutta ei haittaakaan
app.use(bodyParser.urlencoded({extended:true}))

// Tarvitaan, ettÃ¤ voidaan lÃ¤hettÃ¤Ã¤ api request tiettyyn osoitteeseen. 
var request = require('request');

// Kun otetaan yhteys porttiin ilman polkua (Esim. localhost:3000
app.get('/', (req, res) => res.sendFile(__dirname + '/xd.html'))

// Kun otetaan yhteyttÃ¤ osoitteeseen localhost:3000/readData
// Luetaan Muonion dataa 24h ajalta (sovelluksessa varmaan kannattaa kÃ¤yttÃ¤Ã¤ SodankylÃ¤n dataa)
app.get('/readData', function (req, res) { 
	console.log("Reading data");

	request({ uri: 'http://space.fmi.fi/image/realtime/UT/SOD/SODdata_01.txt' }, function (error, response, body) {  
		if (error && response.statusCode !== 200) {
			console.log('Error when contacting the internet address')
		}
		var dataArray = [];
		
		// Splitataan data. ErotinmerkkinÃ¤ rivinvaihto

		var str = body.split("\n");
		var i;
		// KÃ¤ydÃ¤Ã¤n data alkio alkiolta lÃ¤pi. HypÃ¤tÃ¤Ã¤n kaksi ensimmÃ¤istÃ¤ alkiota yli, koska ovat vain otsikkotietoa
		// Varsinainen data alkaa kolmannelta riviltÃ¤ eli 2. alkiosta
		for (i=360;i<(str.length-1); i++)
		{
			// RivillÃ¤ 4 tyhjÃ¤Ã¤ vÃ¤lilyÃ¶ntiÃ¤ erottaa alkuosan rivistÃ¤ ja loppuosan
			var dataLine = str[i].split("    ");
			//console.log("alkuosa = " + dataLine[0]);
			//console.log("loppuosa = " + dataLine[1]);
			
			// Loppuosa splitataan. VÃ¤lilyÃ¶nti erotinmerkkinÃ¤. 
			var auroraData = dataLine[1].split(" ");
			
			console.log("MUO X = " + auroraData[0]);
			console.log("MUO Y = " + auroraData[2]);
			console.log("MUO Z = " + auroraData[3]);
			
			// TEhdÃ¤Ã¤n objekti, johon tallennetaan tÃ¤rkeÃ¤ data kyseiseltÃ¤ riviltÃ¤.
			var data = { x: auroraData[0], y: auroraData[2], z: auroraData[3]};
			
			// LisÃ¤tÃ¤Ã¤n data array:hin
			dataArray.push(data);
		}
		// LÃ¤hetetÃ¤Ã¤n array (eli javascript objekti) vastauksena. 
		res.send(dataArray);
		MongoClient.connect(url, function(err, db) 
		{
		if (err) throw err;
		var dbo = db.db("aurora");
		var myobj =
		{
			"x": auroraData[0],
			"y": auroraData[2],
			"z": auroraData[3]
		};
		dbo.collection("siteData").insert(myobj, function(err, res) 
		{
		if (err) throw err;
		console.log(res.insertedCount);
		db.close();
		});
		});
	});
	
});

app.listen(8080, function() { 
	console.log("Listening port 8080")
});