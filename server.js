const compression = require('compression');
const fs = require('fs');
const express = require('express');
const cors = require('cors');
const http = require('http');
const https = require('https');
const socketIO = require('socket.io');
const bodyparser = require('body-parser');


const db = require('monk')('10.135.139.202:27017/aurora');

const collection_measurementData = db.get('measurementData');
const collection_fmiData = db.get('fmiData');

//const collection_chatData = db.get('chatData');


const fmiData = require('./em-data/fmi-data');
console.log(fmiData);


const port = process.env.PORT || 3000;

const app = express();
app.use(cors());
app.use(compression());
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

const privateKey = fs.readFileSync('/etc/letsencrypt/live/northerneye.live/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/northerneye.live/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/northerneye.live/chain.pem', 'utf8');

const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};


const server = http.createServer(app);
const httpServer = http.createServer(app);
const httpsServer = https.createServer(credentials, app);

const io = socketIO(httpsServer);

io.on("connection", socket => {
    //console.log("New client connected" + socket.id);
    //console.log(socket);


    socket.on("initial_data", () => {
        setInterval(function() {
            collection_measurementData.find({}, {limit: 5, sort: {_id: -1}}).then(docs => {
                io.sockets.emit("get_data", docs);
            });
        }, 10000);
        setInterval(function() {
            collection_fmiData.find({}, {limit: 5, sort: {_id: -1}}).then(data => {
                io.sockets.emit('fmi_data', data)
            });
        }, 5000);
    });


    /*
    socket.on("putOrder", order => {
        collection_measurementData
            .update({ _id: order._id }, { $inc: { ordQty: order.order } })
            .then(updatedDoc => {
                io.sockets.emit("change_data");
            });
    });
    */
    /*
    socket.on("mark_done", id => {
        collection_measurementData
            .update({ _id: id }, { $inc: { ordQty: -1, prodQty: 1 } })
            .then(updatedDoc => {
                io.sockets.emit("change_data");
            });
    });
    */
    /*
    socket.on("ChangePred", predicted_data => {
        collection_measurementData
            .update(
                { _id: predicted_data._id },
                { $set: { predQty: predicted_data.predQty } }
            )
            .then(updatedDoc => {
                io.sockets.emit("change_data");
            });
    });
    */

    socket.on("disconnect", () => {
        console.log("user disconnected");
    });
});

require('dotenv').config();

const axios = require('axios');

const fetchForecast = (key, params) => {
    return axios.get(`https://api.darksky.net/forecast/${key}/${params}?units=auto&exclude=currently,minutely,daily,alerts,flags`,
    ).then(data => data.data['hourly']).catch(err => console.log(err))
};

const getForecast = (params) => {
    return fetchForecast(process.env.APIKEY, params)
};

app.get('/weatherAPI/:coords', (req, res) => {
    getForecast(req.params.coords)
        .then(forecast => {
            res.send(forecast)
        })

});



app.use(express.static("public"));

server.listen(port, () => console.log(`Listening on port ${port}`));

httpServer.listen(80, () => {
    console.log('HTTP server running at 80.')
});

httpsServer.listen(443, () => {
    console.log('HTTPS server running at 443.')
});



