const axios = require('axios');
const monk = require('monk');
const collection = require('../database/config').collection_fmiData;
const bodyParser = require('body-parser');

setInterval(function () {
    axios.get('http://space.fmi.fi/image/realtime/UT/SOD/SODdata_01.txt',)
        .then(function(response) {

            let str = response.data.split('\n');


            for(let i = 360; i < (str.length - 1); i++) {

                let dataLine = str[i].split('    ');

                let auroraData = dataLine[1].split(' ');

                let slopeLine = str[359].split('    ');

                let slopeAuroraData = slopeLine[1].split(' ');

                let auroraDateData = dataLine[0].split(' ');

                let auroraDate = (
                    auroraDateData[0] + '-' +
                    auroraDateData[1] + '-' +
                    auroraDateData[2] + 'T' +
                    auroraDateData[3] + ':' +
                    auroraDateData[4] + ':' +
                    auroraDateData[5] + 'Z'
            );

                let data = { x: auroraData[0], y: auroraData[2], z: auroraData[3]};
                let slopeData = { xSlope: slopeAuroraData[0], ySlope: slopeAuroraData[2], zSlope: slopeAuroraData[3]};


                let totalMf = Math.sqrt(Math.pow(data.x, 2) + Math.pow(data.y, 2) + Math.pow(data.z, 2));
                let totalSlope = Math.sqrt(Math.pow(slopeData.xSlope, 2) + Math.pow(slopeData.ySlope, 2) + Math.pow(slopeData.zSlope, 2));

                console.log(totalMf);
                console.log(totalSlope);

                let slope = (totalMf - totalSlope) / 10;

                console.log(slope);

                console.log(new Date(auroraDate));
                return collection.insert({ time: new Date(auroraDate), totalMf: totalMf, oldTotalMf: totalSlope, slope: slope })
            }

        })
        .catch(err => console.log(err))
}, 5000);


let fmiArray;
