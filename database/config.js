const db = require('monk')('10.135.139.202:27017/aurora');

const collection_measurementData = db.get('measurementData');
const collection_fmiData = db.get('fmiData');

//const collection_chatData = db.get('chatData');

module.exports = { collection_measurementData, collection_fmiData };